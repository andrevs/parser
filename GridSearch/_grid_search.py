
from datetime import datetime
import os
import subprocess


config_base = open('config_base.txt', 'r').read()


def run(id, form, lemma, postag, features, head, deprel):
    config = open('config.txt', 'w')
    param = '--fmc-coefficients'
    values = '%.3f\n%.3f\n%.3f\n%.3f\n%.3f\n%.3f\n%.3f\n' % (id, form, lemma, postag, features, head, deprel)
    config.write(config_base + '\n%s\n%s\n' % (param, values))
    config.close()
    devnull = open(os.devnull, 'w')
    subprocess.call('parser.exe', stdout=devnull, stderr=devnull)
    subprocess.call('diff.exe test.txt parsed.txt diff.txt', stdout=devnull, stderr=devnull)
    diff = filter(None, open('diff.txt', 'r').read().split('\n'))
    sentences_accuracy = float(diff[2].split(' ')[-2])
    tokens_accuracy    = float(diff[5].split(' ')[-2])
    heads_accuracy     = float(diff[7].split(' ')[-2])
    deprels_accuracy   = float(diff[9].split(' ')[-2])
    return [sentences_accuracy, tokens_accuracy, heads_accuracy, deprels_accuracy]


def normalize(coeffs):
    s = sum(coeffs)
    for i in range(len(coeffs)):
        coeffs[i] /= s
    return coeffs


def to_string(coeffs, result):
    return ' '.join('%.3f' % c for c in coeffs) + '\t' + ' '.join('%.3f' % r for r in result)


def info(gen, i, c):
    now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    return '%2d  %s  %.3f\t%s' % (gen, str(i), c, now)


# [id, form, lemma, postag, features, head, deprel]
coeffs = normalize([1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0])
changed = True
delta = 0.5
gen = 0


while changed:
    changed = False
    result = run(*coeffs)
    with open('_log.txt', 'a') as log:
        log.write(to_string(coeffs, result) + '\t' + info(gen, '-', 1.0) + '\n')
    print to_string(coeffs, result)
    cur_error = result[1]
    cur_coeffs = list(coeffs)
    for i in range(len(cur_coeffs)):
        plus_coeffs = list(coeffs)
        plus_coeffs[i] *= 1 + delta
        plus_result = run(*normalize(plus_coeffs))
        plus_error = plus_result[1]
        with open('_log.txt', 'a') as log:
            log.write(to_string(plus_coeffs, plus_result) + '\t' + info(gen, i, 1 + delta) + '\n')
        minus_coeffs = list(coeffs)
        minus_coeffs[i] *= 1 - delta
        minus_result = run(*normalize(minus_coeffs))
        minus_error = minus_result[1]
        with open('_log.txt', 'a') as log:
            log.write(to_string(minus_coeffs, minus_result) + '\t' + info(gen, i, 1 - delta) + '\n')
        if plus_error + 1e-6 < min(cur_error, minus_error):
            cur_coeffs[i] *= 1 + delta
            changed = True
        if minus_error + 1e-6 < min(cur_error, plus_error):
            cur_coeffs[i] *= 1 - delta
            changed = True
    coeffs = normalize(cur_coeffs)
    delta *= 0.9
    gen += 1
