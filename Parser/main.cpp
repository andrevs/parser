
#include <clocale>
#include <cstdlib>
#include <exception>
#include <iomanip>
#include <ios>
#include <iostream>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

#include "Covington.h"
#include "Nivre.h"
#include "Stack.h"

#include "Parameter.h"
#include "Sentence.h"


int main(int argc, char *argv[]) {

	try {

		setlocale(LC_CTYPE, "");

		Parameter parameter("config.txt");
		parameter.readCommandLineArgs(argc, argv);

		AlgorithmBase *parser;

		std::string parsingAlgorithm = parameter.getParamAsString("algorithm-choose");

		if (parsingAlgorithm == "covington") {
			parser = new Covington(parameter);
		}
		else if (parsingAlgorithm == "nivre") {
			parser = new Nivre(parameter);
		}
		else if (parsingAlgorithm == "stack") {
			parser = new Stack(parameter);
		}
		else {
			std::cerr << "invalid parsing algorithm" << std::endl;
			exit(1);
		}

		if (parameter.paramIsDefined("load-svm-model"))
			parser->classifier.loadModel(parameter.getParamAsString("load-svm-model"));

		if (parameter.paramIsDefined("train-file"))
			parser->add(parameter.getParamAsString("train-file"));

		if (parameter.paramIsDefined("classifier-cross-validation")) {
			parser->classifier.setSvmProblem();
			int nFolds = parameter.getParamAsInt("classifier-cross-validation");
			double score = parser->classifier.crossValidationAccuracyScore(nFolds);
			std::cout << "Cross validation accuracy score: " << std::fixed << std::setprecision(3) << score << std::endl;
		}

		if (parameter.paramIsDefined("classifier-liblinear-find-best-c")) {
			parser->classifier.setSvmProblem();
			std::vector<std::string> param = parameter.getParamAsVectorOfStrings("classifier-liblinear-find-best-c");
			int nFolds = atoi(param.at(0).c_str());
			double minC = atof(param.at(1).c_str());
			double maxC = atof(param.at(2).c_str());
			std::pair<double, double> result = parser->classifier.findBestC(nFolds, minC, maxC);
			std::cout << "bestC    = " << std::fixed << std::setprecision(3) << result.first  << std::endl;
			std::cout << "bestRate = " << std::fixed << std::setprecision(3) << result.second << std::endl;
		}

		if (parameter.paramIsDefined("train-file"))
			parser->classifier.fit(true);

		Sentences sentences;
		if (parameter.paramIsDefined("parse-file"))
			sentences = parser->parse(parameter.getParamAsString("parse-file"));
		if (parameter.paramIsDefined("save-parsed"))
			writeSentences(parameter.getParamAsString("save-parsed"), sentences);

		if (parameter.paramIsDefined("save-svm-model"))
			parser->classifier.saveModel(parameter.getParamAsString("save-svm-model"));

	}
	catch (std::exception &exception) {

		std::cout << exception.what() << std::endl;

	}

	return 0;

}
