#pragma once

#include <algorithm>
#include <iostream>
#include <limits>
#include <map>
#include <stdexcept>
#include <string>
#include <vector>


std::vector<std::string> split(const std::string &str, char delim = ' ', int n = INT_MAX, bool skipEmptyParts = true);


std::string intToString(int num);


std::vector<int> stringToVectorOfInts(const std::string &str, char delim = ' ');


std::vector<double> stringToVectorOfDoubles(const std::string &str, char delim = ' ');


std::vector<std::pair<int, double> > stringToSparseArray(const std::string &str, char delim = ' ');


template<typename R, typename T>
R *vectorToArray(const std::vector<T> &vec) {
	if (vec.empty())
		return NULL;
	R *ans = new R[vec.size()];
	for (size_t i = 0; i < vec.size(); ++i)
		ans[i] = vec[i];
	return ans;
}


template<typename T>
T *vectorToArray(const std::vector<T> &vec) {
	return vectorToArray<T, T>(vec);
}


template<typename K, typename T, typename TArg>
const T &at(const std::map<K, T> &map, const TArg &key) {
	typename std::map<K, T>::const_iterator it = map.find(key);
	if (it == map.end())
		throw std::out_of_range("invalid map<K, T> key");
	return it->second;
}


int utf8SymbolToUnicode(const std::string &str, size_t &i);


std::vector<int> utf8ToUnicode(const std::string &str);


std::string unicodeSymbolToUtf8(int symbol);


std::string unicodeToUtf8(const std::vector<int> &unicode);


template<typename T>
std::string transform(const std::string &str, T (*func)(T arg)) {
	std::vector<int> vec = utf8ToUnicode(str);
	std::transform(vec.begin(), vec.end(), vec.begin(), func);
	return unicodeToUtf8(vec);
}
