
#include "Stack.h"

#include "Action.h"
#include "AlgorithmBase.h"
#include "Parameter.h"
#include "Sentence.h"


Stack::Stack(const Parameter &parameter)
	: AlgorithmBase(parameter), allowSwapTransition(false) {
	if (parameter.paramIsDefined("stack-allow-swap-transition"))
		allowSwapTransition = (parameter.getParamAsInt("stack-allow-swap-transition") != 0);
}


Stack::~Stack() {
}


Actions Stack::simulate(const Sentence &s) const {
	Sentence curS = (parseOnlyWords ? s.getWords().normalizeIds() : s);
	Sentence cur(curS);
	for (Sentence::Iterator it = cur.begin(); it != cur.end(); ++it) {
		it->head = 0;
		it->deprel = "_";
	}
	Actions ans;
	Stack::State state(cur);
	while (!state.isTerminative())
		_simulateTransition(ans, curS, cur, state);
	return ans;
}


Sentence Stack::parse(const Sentence &s) const {
	Sentence cur( (parseOnlyWords ? s.getWords().normalizeIds() : s) );
	for (Sentence::Iterator it = cur.begin(); it != cur.end(); ++it) {
		it->head = 0;
		it->deprel = "_";
	}
	Stack::State state(cur);
	while (!state.isTerminative())
		_makeTransition(cur, state);
	return (parseOnlyWords ? s.replaceWords(cur) : cur);
}


// PRIVATE

Stack::State::State(const Sentence &s) {
	for (Sentence::ConstReverseIterator it = s.rBegin(); it != s.rEnd(); ++it)
		input.push_back(it->id);
}


bool Stack::State::isTerminative() const {
	return input.empty();
}


void Stack::_simulateTransition(Actions &ans, const Sentence &s, Sentence &cur, State &state) const {
	if (state.stack.size() > 1 && _simulateLink(ans, s, cur, state.stack.back(), *(state.stack.end() - 2), state))
		_leftArc(state);
	else if (state.stack.size() > 1 && _simulateLink(ans, s, cur, *(state.stack.end() - 2), state.stack.back(), state))
		_rightArc(state);
	else if (allowSwapTransition && state.stack.size() > 1 && (*(state.stack.end() - 2) < state.stack.back()))
		_swap(state);
	else
		_shift(state);
}


void Stack::_makeTransition(Sentence &cur, State &state) const {
	if (state.stack.size() > 1 && _link(cur, state.stack.back(), *(state.stack.end() - 2), state))
		_leftArc(state);
	else if (state.stack.size() > 1 && _link(cur, *(state.stack.end() - 2), state.stack.back(), state))
		_rightArc(state);
	else if (allowSwapTransition && state.stack.size() > 1 && (*(state.stack.end() - 2) < state.stack.back()))
		_swap(state);
	else
		_shift(state);
}


void Stack::_leftArc(State &state) {
	state.stack.erase(state.stack.end() - 2);
}


void Stack::_rightArc(State &state) {
	state.stack.pop_back();
}


void Stack::_swap(State &state) {
	state.input.push_back(*(state.stack.end() - 2));
	state.stack.erase(state.stack.end() - 2);
}


void Stack::_shift(State &state) {
	state.stack.push_back(state.input.back());
	state.input.pop_back();
}


bool Stack::_simulateLink(Actions &ans, const Sentence &s, Sentence &cur, int fromId, int toId, const State &state) const {
	FeatureModel::LinkFeatures features = featureModel(cur, fromId, toId, state.stack, state.input);
	bool makeLink = (s.getTokenById(toId).head == fromId);
	std::string deprel = "_";
	if (makeLink) {
		deprel = s.getTokenById(toId).deprel;
		cur.makeLink(fromId, toId, deprel);
	}
	ans.push_back(Action(features, makeLink, deprel));
	return makeLink;
}


bool Stack::_link(Sentence &cur, int fromId, int toId, const State &state) const {
	FeatureModel::LinkFeatures features = featureModel(cur, fromId, toId, state.stack, state.input);
	FeatureModel::Label label = classifier.predict(features);
	if (label.first)
		cur.makeLink(fromId, toId, label.second);
	return label.first;
}
