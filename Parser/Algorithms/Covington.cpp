
#include "Covington.h"

#include <limits>
#include <map>
#include <string>

#include "Action.h"
#include "AlgorithmBase.h"
#include "Classifier.h"
#include "FeatureModel.h"
#include "Parameter.h"
#include "Sentence.h"


Covington::Covington(const Parameter &parameter)
	: AlgorithmBase(parameter) {
	if (parameter.paramIsDefined("covington-max-number-of-rejects"))
		maxNumberOfRejects = parameter.getParamAsInt("covington-max-number-of-rejects");
	else
		maxNumberOfRejects = INT_MAX;
}


Covington::~Covington() {
}


Actions Covington::simulate(const Sentence &s) const {
	Sentence curS = (parseOnlyWords ? s.getWords().normalizeIds() : s);
	Sentence cur(curS);
	for (Sentence::Iterator it = cur.begin(); it != cur.end(); ++it) {
		it->head = 0;
		it->deprel = "_";
	}
	Actions ans;
	std::map<int, int> numberOfRejects;
	for (Sentence::Iterator u = cur.begin(); u != cur.end(); ++u)
		for (Sentence::ReverseIterator v(u); v != cur.rEnd(); ++v) {
			_tryToSimulateLink(ans, numberOfRejects, curS, cur, u->id, v->id);
			_tryToSimulateLink(ans, numberOfRejects, curS, cur, v->id, u->id);
		}
	return ans;
}


Sentence Covington::parse(const Sentence &s) const {
	Sentence cur( (parseOnlyWords ? s.getWords().normalizeIds() : s) );
	for (Sentence::Iterator it = cur.begin(); it != cur.end(); ++it) {
		it->head = 0;
		it->deprel = "_";
	}
	for (Sentence::Iterator u = cur.begin(); u != cur.end(); ++u)
		for (Sentence::ReverseIterator v(u); v != cur.rEnd(); ++v) {
			_tryToLink(cur, u->id, v->id);
			_tryToLink(cur, v->id, u->id);
		}
	return (parseOnlyWords ? s.replaceWords(cur) : cur);
}


// PRIVATE

Action Covington::_simulateLink(const Sentence &s, Sentence &cur, int fromId, int toId) const {
	FeatureModel::LinkFeatures features = featureModel(cur, fromId, toId);
	bool makeLink = (s.getTokenById(toId).head == fromId);
	std::string deprel = "_";
	if (makeLink) {
		deprel = s.getTokenById(toId).deprel;
		cur.makeLink(fromId, toId, deprel);
	}
	return Action(features, makeLink, deprel);
}


void Covington::_tryToSimulateLink(Actions &ans, std::map<int, int> &nr, const Sentence &s, Sentence &cur, int fromId, int toId) const {
	if (cur.linkIsPermissible(fromId, toId)) {
		Action temp = _simulateLink(s, cur, fromId, toId);
		if (temp.label.first)
			ans.push_back(temp);
		else if (nr[fromId] < maxNumberOfRejects && nr[toId] < maxNumberOfRejects) {
			ans.push_back(temp);
			++nr[fromId];
			++nr[toId];
		}
	}
}


void Covington::_link(Sentence &cur, int fromId, int toId) const {
	FeatureModel::LinkFeatures features = featureModel(cur, fromId, toId);
	FeatureModel::Label label = classifier.predict(features);
	if (label.first)
		cur.makeLink(fromId, toId, label.second);
}


void Covington::_tryToLink(Sentence &cur, int fromId, int toId) const {
	if (cur.linkIsPermissible(fromId, toId))
		_link(cur, fromId, toId);
}
