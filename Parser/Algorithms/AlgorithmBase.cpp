
#include "AlgorithmBase.h"

#include <string>

#include "Action.h"
#include "Classifier.h"
#include "FeatureModel.h"
#include "Parameter.h"
#include "Sentence.h"


AlgorithmBase::AlgorithmBase(const Parameter &parameter)
	: featureModel(parameter), classifier(parameter) {
	parseOnlyWords = (parameter.getParamAsInt("algorithm-parse-only-words") != 0);
}


AlgorithmBase::~AlgorithmBase() {
}


Actions AlgorithmBase::simulate(const Sentences &sentences) const {
	Actions ans;
	for (Sentences::const_iterator it = sentences.begin(); it != sentences.end(); ++it) {
		Actions temp = simulate(*it);
		ans.insert(ans.end(), temp.begin(), temp.end());
	}
	return ans;
}


Actions AlgorithmBase::simulate(const std::string &path) const {
	return simulate(readSentences(path));
}


Sentences AlgorithmBase::parse(const Sentences &sentences) const {
	Sentences ans;
	for (Sentences::const_iterator it = sentences.begin(); it != sentences.end(); ++it)
		ans.push_back(parse(*it));
	return ans;
}


Sentences AlgorithmBase::parse(const std::string &path) const {
	return parse(readSentences(path));
}


void AlgorithmBase::add(const Sentence &s) {
	classifier.clear();
	classifier.add(simulate(s));
}


void AlgorithmBase::add(const Sentences &sentences) {
	classifier.clear();
	classifier.add(simulate(sentences));
}


void AlgorithmBase::add(const std::string &path) {
	classifier.clear();
	classifier.add(simulate(path));
}
