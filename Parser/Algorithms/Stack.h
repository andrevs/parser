#pragma once

#include "Action.h"
#include "AlgorithmBase.h"
#include "Parameter.h"
#include "Sentence.h"


class Stack : public AlgorithmBase {
public:
	Stack(const Parameter &parameter);

	~Stack();

	Actions simulate(const Sentence &s) const;

	Sentence parse(const Sentence &s) const;

	bool allowSwapTransition;

private:
	class State {
	public:
		State(const Sentence &s);

		bool isTerminative() const;

		std::vector<int> stack;
		std::vector<int> input;

	};

	void _simulateTransition(Actions &ans, const Sentence &s, Sentence &cur, State &state) const;

	void _makeTransition(Sentence &cur, State &state) const;

	static void _leftArc(State &state);

	static void _rightArc(State &state);

	static void _swap(State &state);

	static void _shift(State &state);

	bool _simulateLink(Actions &ans, const Sentence &s, Sentence &cur, int fromId, int toId, const State &state) const;

	bool _link(Sentence &cur, int fromId, int toId, const State &state) const;

};
