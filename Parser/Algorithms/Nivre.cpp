
#include "Nivre.h"

#include <set>
#include <vector>

#include "Action.h"
#include "AlgorithmBase.h"
#include "Parameter.h"
#include "Sentence.h"


Nivre::Nivre(const Parameter &parameter)
	: AlgorithmBase(parameter) {
}


Nivre::~Nivre() {
}


Actions Nivre::simulate(const Sentence &s) const {
	Sentence curS = (parseOnlyWords ? s.getWords().normalizeIds() : s);
	Sentence cur(curS);
	for (Sentence::Iterator it = cur.begin(); it != cur.end(); ++it) {
		it->head = 0;
		it->deprel = "_";
	}
	Actions ans;
	Nivre::State state(cur);
	while (!state.isTerminative())
		_simulateTransition(ans, curS, cur, state);
	return ans;
}


Sentence Nivre::parse(const Sentence &s) const {
	Sentence cur( (parseOnlyWords ? s.getWords().normalizeIds() : s) );
	for (Sentence::Iterator it = cur.begin(); it != cur.end(); ++it) {
		it->head = 0;
		it->deprel = "_";
	}
	Nivre::State state(cur);
	while (!state.isTerminative())
		_makeTransition(cur, state);
	return (parseOnlyWords ? s.replaceWords(cur) : cur);
}


// PRIVATE

Nivre::State::State(const Sentence &s) {
	for (Sentence::ConstReverseIterator it = s.rBegin(); it != s.rEnd(); ++it)
		input.push_back(it->id);
}


bool Nivre::State::isTerminative() const {
	return input.empty();
}


void Nivre::_simulateTransition(Actions &ans, const Sentence &s, Sentence &cur, State &state) const {
	int stackTop = (!state.stack.empty() ? state.stack.back() : -1);
	int inputTop = state.input.back();
	if (!state.stack.empty() && (state.arcToIds.count(stackTop) == 0) && _simulateLink(ans, s, cur, inputTop, stackTop, state))
		_leftArc(state);
	else if (!state.stack.empty() && (state.arcToIds.count(inputTop) == 0) && _simulateLink(ans, s, cur, stackTop, inputTop, state))
		_rightArc(state);
	else if (!state.stack.empty() && (state.arcToIds.count(stackTop) > 0))
		_reduce(state);
	else
		_shift(state);
}


void Nivre::_makeTransition(Sentence &cur, State &state) const {
	int stackTop = (!state.stack.empty() ? state.stack.back() : -1);
	int inputTop = state.input.back();
	if (!state.stack.empty() && (state.arcToIds.count(stackTop) == 0) && _link(cur, inputTop, stackTop, state))
		_leftArc(state);
	else if (!state.stack.empty() && (state.arcToIds.count(inputTop) == 0) && _link(cur, stackTop, inputTop, state))
		_rightArc(state);
	else if (!state.stack.empty() && (state.arcToIds.count(stackTop) > 0))
		_reduce(state);
	else
		_shift(state);
}


void Nivre::_leftArc(State &state) {
	state.arcToIds.insert(state.stack.back());
	state.stack.pop_back();
}


void Nivre::_rightArc(State &state) {
	state.arcToIds.insert(state.input.back());
	state.stack.push_back(state.input.back());
	state.input.pop_back();
}


void Nivre::_reduce(State &state) {
	state.stack.pop_back();
}


void Nivre::_shift(State &state) {
	state.stack.push_back(state.input.back());
	state.input.pop_back();
}


bool Nivre::_simulateLink(Actions &ans, const Sentence &s, Sentence &cur, int fromId, int toId, const State &state) const {
	FeatureModel::LinkFeatures features = featureModel(cur, fromId, toId, state.stack, state.input);
	bool makeLink = (s.getTokenById(toId).head == fromId);
	std::string deprel = "_";
	if (makeLink) {
		deprel = s.getTokenById(toId).deprel;
		cur.makeLink(fromId, toId, deprel);
	}
	ans.push_back(Action(features, makeLink, deprel));
	return makeLink;
}


bool Nivre::_link(Sentence &cur, int fromId, int toId, const State &state) const {
	FeatureModel::LinkFeatures features = featureModel(cur, fromId, toId, state.stack, state.input);
	FeatureModel::Label label = classifier.predict(features);
	if (label.first)
		cur.makeLink(fromId, toId, label.second);
	return label.first;
}
