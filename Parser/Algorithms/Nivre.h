#pragma once

#include <set>
#include <vector>

#include "Action.h"
#include "AlgorithmBase.h"
#include "Parameter.h"
#include "Sentence.h"


class Nivre : public AlgorithmBase {
public:
	Nivre(const Parameter &parameter);

	~Nivre();

	Actions simulate(const Sentence &s) const;

	Sentence parse(const Sentence &s) const;

private:
	class State {
	public:
		State(const Sentence &s);

		bool isTerminative() const;

		std::vector<int> stack;
		std::vector<int> input;
		std::set<int> arcToIds;

	};

	void _simulateTransition(Actions &ans, const Sentence &s, Sentence &cur, State &state) const;

	void _makeTransition(Sentence &cur, State &state) const;

	static void _leftArc(State &state);

	static void _rightArc(State &state);

	static void _reduce(State &state);

	static void _shift(State &state);

	bool _simulateLink(Actions &ans, const Sentence &s, Sentence &cur, int fromId, int toId, const State &state) const;

	bool _link(Sentence &cur, int fromId, int toId, const State &state) const;

};
