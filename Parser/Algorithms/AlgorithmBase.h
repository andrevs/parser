#pragma once

#include <string>

#include "Action.h"
#include "Classifier.h"
#include "FeatureModel.h"
#include "Parameter.h"
#include "Sentence.h"


class AlgorithmBase {
protected:
	AlgorithmBase(const Parameter &parameter);

public:
	virtual ~AlgorithmBase();

	virtual Actions simulate(const Sentence &s) const = 0;

	Actions simulate(const Sentences &sentences) const;

	Actions simulate(const std::string &path) const;

	virtual Sentence parse(const Sentence &s) const = 0;

	Sentences parse(const Sentences &sentences) const;

	Sentences parse(const std::string &path) const;

	void add(const Sentence &s);

	void add(const Sentences &sentences);

	void add(const std::string &path);

	FeatureModel featureModel;
	Classifier classifier;

	bool parseOnlyWords;

};
