#pragma once

#include <map>

#include "Action.h"
#include "AlgorithmBase.h"
#include "Parameter.h"
#include "Sentence.h"


class Covington: public AlgorithmBase {
public:
	Covington(const Parameter &parameter);

	~Covington();

	Actions simulate(const Sentence &s) const;

	Sentence parse(const Sentence &s) const;

	int maxNumberOfRejects;

private:
	Action _simulateLink(const Sentence &s, Sentence &cur, int fromId, int toId) const;

	void _tryToSimulateLink(Actions &ans, std::map<int, int> &nr, const Sentence &s, Sentence &cur, int fromId, int toId) const;

	void _link(Sentence &cur, int fromId, int toId) const;

	void _tryToLink(Sentence &cur, int fromId, int toId) const;

};
