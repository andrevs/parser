#pragma once

#include <map>
#include <set>
#include <string>
#include <vector>

#include "Functions.h"
#include "Parameter.h"
#include "Sentence.h"


class FeatureModel {
public:
	typedef std::string Feature;
	typedef std::vector<Feature> Features;
	typedef std::map<std::string, Features> TokenFeatures;
	typedef std::vector<TokenFeatures> LinkFeatures;

	typedef std::pair<bool, std::string> Label;
	typedef std::vector<Label> Labels;

	FeatureModel(const Parameter &parameter);

	LinkFeatures operator()(const Sentence &s, int fromID, int toID) const;

	LinkFeatures operator()(const Sentence &s, int fromID, int toID, const std::vector<int> &stack, const std::vector<int> &input) const;

private:

	class _TokenModel {
	public:
		_TokenModel(const std::string &str);

		std::string id;
		int offset;
		std::set<std::string> pattern;

	};

	std::vector<_TokenModel> _model;

	TokenFeatures operator()(const Sentence &s, int id, const std::set<std::string> &pattern) const;
	
};
