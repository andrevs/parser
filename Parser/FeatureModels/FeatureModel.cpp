
#include "FeatureModel.h"

#include <cstdlib>
#include <set>
#include <string>
#include <vector>

#include "Functions.h"
#include "Parameter.h"
#include "Sentence.h"


FeatureModel::FeatureModel(const Parameter &parameter) {
	std::vector<std::string> temp = parameter.getParamAsVectorOfStrings("feature-model");
	for (std::vector<std::string>::const_iterator it = temp.begin(); it != temp.end(); ++it)
		_model.push_back(_TokenModel(*it));
}


FeatureModel::LinkFeatures FeatureModel::operator()(const Sentence &s, int fromID, int toID) const {
	LinkFeatures ans;
	for (std::vector<_TokenModel>::const_iterator it = _model.begin(); it != _model.end(); ++it)
		if (it->id == "from" || it->id == "to") {
			int id = (it->id == "from" ? fromID : toID) + it->offset;
			TokenFeatures temp = (*this)(s, id, it->pattern);
			ans.push_back(temp);
		}
	return ans;
}


FeatureModel::LinkFeatures FeatureModel::operator()(const Sentence &s, int fromID, int toID, const std::vector<int> &stack, const std::vector<int> &input) const {
	LinkFeatures ans;
	for (std::vector<_TokenModel>::const_iterator it = _model.begin(); it != _model.end(); ++it) {
		int id = -1;
		if (it->id == "from" || it->id == "to")
			id = (it->id == "from" ? fromID : toID) + it->offset;
		else if (it->id == "stack" || it->id == "input") {
			const std::vector<int> &source = (it->id == "stack" ? stack : input);
			int pos = source.size() - 1 + it->offset;
			if (0 <= pos && pos < source.size())
				id = source[pos];
		}
		TokenFeatures temp = (*this)(s, id, it->pattern);
		ans.push_back(temp);
	}
	return ans;
}


// PRIVATE

FeatureModel::_TokenModel::_TokenModel(const std::string &str) {
	std::vector<std::string> splitted = split(str, '\t');
	id = splitted[0];
	offset = atoi(splitted[1].c_str());
	std::vector<std::string> patternVector = split(splitted[2]);
	pattern.insert(patternVector.begin(), patternVector.end());
}


FeatureModel::TokenFeatures FeatureModel::operator()(const Sentence &s, int id, const std::set<std::string> &pattern) const {
	TokenFeatures ans;
	if (s.idIsValid(id)) {
		const Sentence::Token &token = s.getTokenById(id);
		if (pattern.find("id") != pattern.end())
			ans["id"].push_back(intToString(token.id));
		if (pattern.find("form") != pattern.end() && token.form != "_")
			ans["form"].push_back(token.form);
		if (pattern.find("lemma") != pattern.end() && token.lemma != "_")
			ans["lemma"].push_back(token.lemma);
		if (pattern.find("postag") != pattern.end() && token.postag != "_")
			ans["postag"].push_back(token.postag);
		if (pattern.find("features") != pattern.end() && token.features != "_")
			ans["features"] = split(token.features, '|');
		if (pattern.find("head") != pattern.end() && token.head != 0)
			ans["head"].push_back(intToString(token.head));
		if (pattern.find("deprel") != pattern.end() && token.deprel != "_")
			ans["deprel"].push_back(token.deprel);
	}
	return ans;
}
