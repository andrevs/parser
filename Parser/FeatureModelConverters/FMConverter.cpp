
#include "FMConverter.h"

#include <algorithm>
#include <cwctype>
#include <fstream>
#include <iostream>
#include <iterator>
#include <limits>
#include <map>
#include <set>
#include <string>
#include <vector>

#include "FeatureModel.h"
#include "Functions.h"


FMConverter::FMConverter(const Parameter &parameter) {
	size_t _coeffSize = 7;
	std::string _coeffKeys[] = { "id", "form", "lemma", "postag", "features", "head", "deprel" };
	std::vector<std::string> _coeffStrVals = parameter.getParamAsVectorOfStrings("fmc-coefficients");
	for (size_t i = 0; i < _coeffSize; ++i)
		_coeff[_coeffKeys[i]] = atof(_coeffStrVals[i].c_str());

	_idN = 1;

	bool wordVecIsSparsed = false;
	if (parameter.paramIsDefined("fmc-word-vec-is-sparsed"))
		wordVecIsSparsed = (parameter.getParamAsInt("fmc-word-vec-is-sparsed") != 0);
	_readWordVec(parameter.getParamAsString("fmc-word-vec-file"), wordVecIsSparsed);
	int maxInd = 0;
	for (std::map<std::string, SparseArray>::const_iterator it = _wordVec.begin(); it != _wordVec.end(); ++it)
		if (!it->second.empty())
			maxInd = std::max(maxInd, it->second.rbegin()->first);
	_formN = _lemmaN = maxInd + 1;

	_postagIndex = _setIndex(parameter.getParamAsVectorOfStrings("fmc-postags-list"));
	_postagN = static_cast<int>(_postagIndex.size());

	_featuresIndex = _setIndex(parameter.getParamAsVectorOfStrings("fmc-features-list"));
	_featuresN = static_cast<int>(_featuresIndex.size());

	_headN = 1;

	_deprelIndex = _setIndex(parameter.getParamAsVectorOfStrings("fmc-deprel-list"));
	_deprelN = static_cast<int>(_deprelIndex.size());
	
	size_t _offsetSize = 8;
	std::string _offsetKeys[] = { "id", "form", "lemma", "postag", "features", "head", "deprel", "token" };
	int _keyVals[] = { 0, _idN, _formN, _lemmaN, _postagN, _featuresN, _headN, _deprelN };
	for (size_t i = 0; i < _offsetSize; ++i)
		_offset[_offsetKeys[i]] = (i > 0 ? _offset[_offsetKeys[i - 1]] : 0) + _keyVals[i];

	_labelIndex.insert(FeatureModel::Label(false, "_"), 0);
	_labelIndex.insert(FeatureModel::Label(true, "_"), 1);
	for (std::map<std::string, int>::const_iterator it = _deprelIndex.begin(); it != _deprelIndex.end(); ++it) {
		int ind = static_cast<int>(_labelIndex.size());
		_labelIndex.insert(FeatureModel::Label(true, it->first), ind);
	}
}


FMConverter::DenseArray FMConverter::linkFeaturesToDenseArray(const FeatureModel::LinkFeatures &features, int n) const {
	SparseArray temp = linkFeaturesToSparseArray(features);
	DenseArray ans(n);
	for (SparseArray::const_iterator it = temp.begin(); it != temp.end(); ++it) {
		if (it->first >= static_cast<int>(ans.size()))
			ans.resize(it->first + 1);
		ans[it->first] = it->second;
	}
	return ans;
}


FMConverter::SparseArray FMConverter::linkFeaturesToSparseArray(const FeatureModel::LinkFeatures &features) const {
	SparseArray ans;
	for (size_t i = 0; i < features.size(); ++i)
		_sparseUnion(ans, _tokenFeaturesToSparseArray(features[i]), static_cast<int>(i) * at(_offset, "token") + 1);
	return ans;
}


bool FMConverter::labelIsValid(const FeatureModel::Label &label) const {
	return (_labelIndex.findLeft(label) != _labelIndex.leftEnd());
}


int FMConverter::labelToNum(const FeatureModel::Label &label) const {
	return _labelIndex.getByLeft(label);
}


FeatureModel::Label FMConverter::numToLabel(int num) const {
	return _labelIndex.getByRight(num);
}


// PRIVATE

void FMConverter::_sparseUnion(SparseArray &dest, const SparseArray &source, int offset) {
	for (SparseArray::const_iterator it = source.begin(); it != source.end(); ++it)
		dest.push_back(std::make_pair(it->first + offset, it->second));
}


void FMConverter::_insert(SparseArray &dest, const std::string &type, int rawIndex, double rawValue) const {
	int index = at(_offset, type) + rawIndex;
	double value = rawValue * at(_coeff, type);
	if (std::abs(value) > FLT_EPSILON)
		dest.push_back(std::make_pair(index, value));
}


void FMConverter::_readWordVec(const std::string &path, bool wordVecIsSparsed) {
	std::ifstream file(path.c_str());
	std::string temp((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
	std::vector<std::string> lines = split(temp, '\n');
	for (std::vector<std::string>::const_iterator it = lines.begin(); it != lines.end(); ++it)
		if (wordVecIsSparsed || it != lines.begin()) {
			std::vector<std::string> splitted = split(*it, (wordVecIsSparsed ? '\t' : ' '), 1);
			if (splitted.size() == 2) {
				std::string word = transform(splitted[0], towlower);
				if (_wordVec.find(word) == _wordVec.end()) {
					SparseArray vec;
					if (wordVecIsSparsed) {
						vec = stringToSparseArray(splitted[1]);
					}
					else {
						std::vector<double> temp = stringToVectorOfDoubles(splitted[1]);
						for (size_t i = 0; i < temp.size(); ++i)
							vec.push_back(std::make_pair(static_cast<int>(i), temp[i]));
					}
					_wordVec[word] = vec;
				}
			}
		}
}


std::map<std::string, int> FMConverter::_setIndex(const std::vector<std::string> &list, char delim) {
	std::map<std::string, int> ans;
	for (std::vector<std::string>::const_iterator it = list.begin(); it != list.end(); ++it) {
		std::vector<std::string> splitted = split(*it, delim);
		for (std::vector<std::string>::const_iterator sIt = splitted.begin(); sIt != splitted.end(); ++sIt)
			if (ans.find(*sIt) == ans.end()) {
				int ind = static_cast<int>(ans.size());
				ans[*sIt] = ind;
			}
	}
	return ans;
}


FMConverter::SparseArray FMConverter::_tokenFeaturesToSparseArray(const FeatureModel::TokenFeatures &features) const {
	SparseArray ans;
	
	if (features.find("id") != features.end())
		_insert(ans, "id", 0, atof(at(features, "id").at(0).c_str()));
	
	if (features.find("form") != features.end()) {
		std::string form = transform(at(features, "form").at(0), towlower);
		if (_wordVec.find(form) != _wordVec.end()) {
			SparseArray temp = at(_wordVec, form);
			for (SparseArray::const_iterator it = temp.begin(); it != temp.end(); ++it)
				_insert(ans, "form", it->first, it->second);
		}
		else {
			std::cerr << "Can't find form: \"" << form << "\" in _wordVec." << std::endl;
		}
	}
	
	if (features.find("lemma") != features.end()) {
		std::string lemma = transform(at(features, "lemma").at(0), towlower);
		if (_wordVec.find(lemma) != _wordVec.end()) {
			SparseArray temp = at(_wordVec, lemma);
			for (SparseArray::const_iterator it = temp.begin(); it != temp.end(); ++it)
				_insert(ans, "lemma", it->first, it->second);
		}
		else {
			std::cerr << "Can't find lemma: \"" << lemma << "\" in _wordVec." << std::endl;
		}
	}
	
	if (features.find("postag") != features.end()) {
		std::string postag = at(features, "postag").at(0);
		if (_postagIndex.find(postag) != _postagIndex.end())
			_insert(ans, "postag", at(_postagIndex, postag), 1);
		else
			std::cerr << "Can't find postag: \"" << postag << "\" in _postagIndex." << std::endl;
	}
	
	if (features.find("features") != features.end()) {
		FeatureModel::Features temp = at(features, "features");
		for (size_t i = 0; i < temp.size(); ++i)
			if (_featuresIndex.find(temp[i]) != _featuresIndex.end())
				_insert(ans, "features", at(_featuresIndex, temp[i]), 1);
			else
				std::cerr << "Can't find feature: \"" << temp[i] << "\" in _featuresIndex." << std::endl;
	}
	
	if (features.find("head") != features.end())
		_insert(ans, "head", 0, atof(at(features, "head").at(0).c_str()));

	if (features.find("deprel") != features.end()) {
		std::string deprel = at(features, "deprel").at(0);
		if (_deprelIndex.find(deprel) != _deprelIndex.end())
			_insert(ans, "deprel", at(_deprelIndex, deprel), 1);
		else
			std::cerr << "Can't find deprel: \"" << deprel << "\" in _deprelIndex." << std::endl;
	}

	std::sort(ans.begin(), ans.end());

	return ans;
}
