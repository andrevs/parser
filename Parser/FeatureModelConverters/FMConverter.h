#pragma once

#include <map>
#include <set>
#include <string>
#include <vector>

#include "BiMap.h"
#include "FeatureModel.h"
#include "Parameter.h"


class FMConverter {
public:
	typedef std::vector<double> DenseArray;
	typedef std::vector<std::pair<int, double> > SparseArray;

	FMConverter(const Parameter &parameter);

	DenseArray linkFeaturesToDenseArray(const FeatureModel::LinkFeatures &features, int n = 0) const;

	SparseArray linkFeaturesToSparseArray(const FeatureModel::LinkFeatures &features) const;

	bool labelIsValid(const FeatureModel::Label &label) const;

	int labelToNum(const FeatureModel::Label &label) const;

	FeatureModel::Label numToLabel(int num) const;

private:
	std::map<std::string, double> _coeff;

	int _idN, _formN, _lemmaN, _postagN, _featuresN, _headN, _deprelN;
	std::map<std::string, int> _offset;

	std::map<std::string, SparseArray> _wordVec;
	std::map<std::string, int> _postagIndex;
	std::map<std::string, int> _featuresIndex;
	std::map<std::string, int> _deprelIndex;

	BiMap<FeatureModel::Label, int> _labelIndex;

	static void _sparseUnion(SparseArray &dest, const SparseArray &src, int offset);

	void _insert(SparseArray &dest, const std::string &type, int rawIndex, double rawValue) const;

	void _readWordVec(const std::string &path, bool wordVecIsSparsed);

	static std::map<std::string, int> _setIndex(const std::vector<std::string> &list, char delim = '|');

	SparseArray _tokenFeaturesToSparseArray(const FeatureModel::TokenFeatures &features) const;

};
