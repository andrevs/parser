#pragma once


typedef long long int int64_t;


struct svm_node
{
	int64_t index;
	double value;
};
