
#include "Classifier.h"

#include <algorithm>
#include <cstdlib>
#include <exception>
#include <fstream>
#include <iostream>
#include <set>
#include <string>
#include <vector>

#include "linear.h"
#include "svm.h"

#include "Action.h"
#include "BiMap.h"
#include "Functions.h"
#include "Parameter.h"


Classifier::Classifier(const Parameter &parameter)
	: _converter(parameter),
	  _svmLib(parameter.getParamAsString("classifier-svm-lib")),
	  _libsvmModel(NULL),
	  _libsvmProblem(NULL),
	  _libsvmParameter(NULL),
	  _liblinearModel(NULL),
	  _liblinearProblem(NULL),
	  _liblinearParameter(NULL) {
	_setSvmParameter(parameter);
}


Classifier::~Classifier() {
	_freeSvmModel();
	_freeSvmProblem();
	_freeSvmParameter();
}


void Classifier::add(const FeatureModel::LinkFeatures &x, const FeatureModel::Label &y) {
	if (_converter.labelIsValid(y)) {
		_x.push_back(_converter.linkFeaturesToSparseArray(x));
		_y.push_back(_converter.labelToNum(y));
	}
	else {
		std::cerr << "invalid label: (" << (y.first ? "true" : "false") << ", \"" << y.second << "\")" << std::endl;
	}
}


void Classifier::add(const std::vector<FeatureModel::LinkFeatures> &x, const FeatureModel::Labels &y) {
	std::vector<FeatureModel::LinkFeatures>::const_iterator xIt;
	FeatureModel::Labels::const_iterator yIt;
	for (xIt = x.begin(), yIt = y.begin(); xIt != x.end() && yIt != y.end(); ++xIt, ++yIt)
		add(*xIt, *yIt);
}


void Classifier::add(const Action &action) {
	add(action.features, action.label);
}


void Classifier::add(const Actions &actions) {
	for (std::vector<Action>::const_iterator it = actions.begin(); it != actions.end(); ++it)
		add(*it);
}


void Classifier::setSvmProblem(bool clearAddedData) {
	_setSvmProblem(clearAddedData);
}


void Classifier::fit(bool clearAddedData) {
	_freeSvmModel();
	_setSvmProblem(clearAddedData);
	if (_svmLib == "libsvm") {
		_libsvmModel = svm_train(_libsvmProblem, _libsvmParameter);
		if (_libsvmModel == NULL)
			throw std::logic_error("Failed to train libsvm model");
	}
	else if (_svmLib == "liblinear") {
		_liblinearModel = train(_liblinearProblem, _liblinearParameter);
		if (_liblinearModel == NULL)
			throw std::logic_error("Failed to train liblinear model");
	}
}


void Classifier::clear() {
	_freeSvmModel();
	_freeSvmProblem();
	_x.clear();
	_y.clear();
}


FeatureModel::Label Classifier::predict(const FeatureModel::LinkFeatures &x) const {
	svm_node *arrX = _sparseArrayToArrayOfSvmNodes(_converter.linkFeaturesToSparseArray(x));
	int pred = -1;
	if (_svmLib == "libsvm")
		pred = static_cast<int>(svm_predict(_libsvmModel, arrX) + 0.5);
	else if (_svmLib == "liblinear")
		pred = static_cast<int>(::predict(_liblinearModel, arrX) + 0.5);
	delete[] arrX;
	return _converter.numToLabel(pred);
}


FeatureModel::Labels Classifier::predict(const std::vector<FeatureModel::LinkFeatures> &x) const {
	FeatureModel::Labels y;
	for (std::vector<FeatureModel::LinkFeatures>::const_iterator it = x.begin(); it != x.end(); ++it)
		y.push_back(predict(*it));
	return y;
}


double Classifier::crossValidationAccuracyScore(int nFolds) const {
	int l = static_cast<int>(_x.size());
	double *target = new double[l];
	if (_svmLib == "libsvm")
		svm_cross_validation(_libsvmProblem, _libsvmParameter, nFolds, target);
	else if (_svmLib == "liblinear")
		cross_validation(_liblinearProblem, _liblinearParameter, nFolds, target);
	int n = 0;
	for (int i = 0; i < l; ++i)
		n += (_y[i] == static_cast<int>(target[i] + 0.5));
	return n * 1.0 / l;
}


std::pair<double, double> Classifier::findBestC(int nFolds, double minC, double maxC) const {
	double bestC = 1.0;
	double bestRate = 0.0;
	if (_svmLib == "liblinear")
		find_parameter_C(_liblinearProblem, _liblinearParameter, nFolds, minC, maxC, &bestC, &bestRate);
	return std::make_pair(bestC, bestRate);
}


void Classifier::saveModel(const std::string &path) const {
	if (_svmLib == "libsvm")
		if(svm_save_model(path.c_str(), _libsvmModel) != 0)
			throw std::logic_error("Failed to save libsvm model");
	else if (_svmLib == "liblinear")
		if(save_model(path.c_str(), _liblinearModel) != 0)
			throw std::logic_error("Failed to save liblinear model");
}


void Classifier::loadModel(const std::string &path) {
	_freeSvmModel();
	if (_svmLib == "libsvm") {
		_libsvmModel = svm_load_model(path.c_str());
		if (_libsvmModel == NULL)
			throw std::logic_error("Failed to load libsvm model");
	}
	else if (_svmLib == "liblinear") {
		_liblinearModel = load_model(path.c_str());
		if (_liblinearModel == NULL)
			throw std::logic_error("Failed to load liblinear model");
	}
}


// PRIVATE

void Classifier::_freeSvmModel() {
	if (_libsvmModel != NULL) {
		svm_free_model_content(_libsvmModel);
		svm_free_and_destroy_model(&_libsvmModel);
		_libsvmModel = NULL;
	}
	if (_liblinearModel != NULL) {
		free_model_content(_liblinearModel);
		free_and_destroy_model(&_liblinearModel);
		_liblinearModel = NULL;
	}
}


void Classifier::_freeSvmProblem() {
	if (_libsvmProblem != NULL) {
		for (int i = 0; i < _libsvmProblem->l; ++i)
			delete[] _libsvmProblem->x[i];
		delete[] _libsvmProblem->x;
		delete[] _libsvmProblem->y;
		delete _libsvmProblem;
		_libsvmProblem = NULL;
	}
	if (_liblinearProblem != NULL) {
		for (int i = 0; i < _liblinearProblem->l; ++i)
			delete[] _liblinearProblem->x[i];
		delete[] _liblinearProblem->x;
		delete[] _liblinearProblem->y;
		delete _liblinearProblem;
		_liblinearProblem = NULL;
	}
}


void Classifier::_freeSvmParameter() {
	if (_libsvmParameter != NULL) {
		svm_destroy_param(_libsvmParameter);
		_libsvmParameter = NULL;
	}
	if (_liblinearParameter != NULL) {
		destroy_param(_liblinearParameter);
		_liblinearParameter = NULL;
	}
}


void Classifier::_setSvmProblem(bool clearAddedData) {
	_freeSvmProblem();
	int l = static_cast<int>(_x.size());
	int max_index = 0;
	svm_node **x = new svm_node*[l];
	for (int i = 0; i < l; ++i) {
		if (!_x[i].empty())
			max_index = std::max(max_index, _x[i].back().first);
		x[i] = _sparseArrayToArrayOfSvmNodes(_x[i]);
	}
	double *y = vectorToArray<double, int>(_y);
	if (clearAddedData) {
		_x.clear();
		_y.clear();
	}
	if (_svmLib == "libsvm") {
		_libsvmProblem = new svm_problem();
		_libsvmProblem->l = l;
		_libsvmProblem->x = x;
		_libsvmProblem->y = y;
	}
	else if (_svmLib == "liblinear") {
		_liblinearProblem = new problem();
		_liblinearProblem->bias = -1;
		_liblinearProblem->l = l;
		_liblinearProblem->n = max_index;
		_liblinearProblem->x = x;
		_liblinearProblem->y = y;
	}
	const char *result = NULL;
	if (_svmLib == "libsvm")
		result = svm_check_parameter(_libsvmProblem, _libsvmParameter);
	else if (_svmLib == "liblinear")
		result = check_parameter(_liblinearProblem, _liblinearParameter);
	if (result != NULL)
		throw std::logic_error(std::string("Incorrect svm parameter: ") + result);
}


void Classifier::_setSvmParameter(const Parameter &parameter) {
	_freeSvmParameter();
	if (_svmLib == "libsvm") {
		_libsvmParameter = new svm_parameter();
		std::vector<std::string> param = parameter.getParamAsVectorOfStrings("classifier-libsvm-parameter");
		_libsvmParameter->svm_type = atoi(param.at(0).c_str());
		_libsvmParameter->kernel_type = atoi(param.at(1).c_str());
		_libsvmParameter->degree = atoi(param.at(2).c_str());
		_libsvmParameter->gamma = atof(param.at(3).c_str());
		_libsvmParameter->coef0 = atof(param.at(4).c_str());
		_libsvmParameter->cache_size = atof(param.at(5).c_str());
		_libsvmParameter->eps = atof(param.at(6).c_str());
		_libsvmParameter->C = atof(param.at(7).c_str());
		_libsvmParameter->nr_weight = atoi(param.at(8).c_str());
		_libsvmParameter->weight_label = vectorToArray<long long int, int>(stringToVectorOfInts(param.at(9)));
		_libsvmParameter->weight = vectorToArray(stringToVectorOfDoubles(param.at(10)));
		_libsvmParameter->nu = atof(param.at(11).c_str());
		_libsvmParameter->p = atof(param.at(12).c_str());
		_libsvmParameter->shrinking = atoi(param.at(13).c_str());
		_libsvmParameter->probability = atoi(param.at(14).c_str());
		_libsvmParameter->max_iter = atoi(param.at(15).c_str());
	}
	else if (_svmLib == "liblinear") {
		_liblinearParameter = new ::parameter();
		std::vector<std::string> param = parameter.getParamAsVectorOfStrings("classifier-liblinear-parameter");
		_liblinearParameter->solver_type = atoi(param.at(0).c_str());
		_liblinearParameter->eps = atof(param.at(1).c_str());
		_liblinearParameter->C = atof(param.at(2).c_str());
		_liblinearParameter->nr_weight = atoi(param.at(3).c_str());
		_liblinearParameter->weight_label = vectorToArray<long long int, int>(stringToVectorOfInts(param.at(4)));
		_liblinearParameter->weight = vectorToArray(stringToVectorOfDoubles(param.at(5)));
		_liblinearParameter->p = atof(param.at(6).c_str());
		_liblinearParameter->init_sol = vectorToArray(stringToVectorOfDoubles(param.at(7)));
		_liblinearParameter->max_iter = atoi(param.at(8).c_str());
	}
}


svm_node Classifier::_makeSvmNode(const std::pair<int, double> &pair) {
	svm_node node;
	node.index = pair.first;
	node.value = pair.second;
	return node;
}


svm_node *Classifier::_sparseArrayToArrayOfSvmNodes(const FMConverter::SparseArray &arr) const {
	std::vector<svm_node> vec;
	for (FMConverter::SparseArray::const_iterator it = arr.begin(); it != arr.end(); ++it)
		vec.push_back(_makeSvmNode(*it));
	vec.push_back(_makeSvmNode(std::make_pair(-1, 0)));
	return vectorToArray(vec);
}
