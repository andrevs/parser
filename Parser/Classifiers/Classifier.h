#pragma once

#include <map>
#include <string>
#include <utility>
#include <vector>

#include "linear.h"
#include "svm.h"

#include "Action.h"
#include "FeatureModel.h"
#include "FMConverter.h"
#include "Parameter.h"


class Classifier {
public:

	Classifier(const Parameter &parameter);

	~Classifier();

	void add(const FeatureModel::LinkFeatures &x, const FeatureModel::Label &y);

	void add(const std::vector<FeatureModel::LinkFeatures> &x, const FeatureModel::Labels &y);
	
	void add(const Action &action);

	void add(const Actions &actions);

	void setSvmProblem(bool clearAddedData = false);

	void fit(bool clearAddedData = false);

	void clear();

	FeatureModel::Label predict(const FeatureModel::LinkFeatures &x) const;

	FeatureModel::Labels predict(const std::vector<FeatureModel::LinkFeatures> &x) const;

	// call setSvmProblem() before usage
	double crossValidationAccuracyScore(int nFolds) const;

	// call setSvmProblem() before usage
	// only for liblinear
	// returns std::pair(bestC, bestRate)
	std::pair<double, double> findBestC(int nFolds, double minC, double maxC) const;

	void saveModel(const std::string &path) const;

	void loadModel(const std::string &path);

private:
	Classifier(const Classifier &classifier);

	FMConverter _converter;

	std::vector<FMConverter::SparseArray> _x;
	std::vector<int> _y;

	std::string _svmLib;

	// libsvm
	svm_model *_libsvmModel;
	svm_problem *_libsvmProblem;
	svm_parameter *_libsvmParameter;

	// liblinear
	model *_liblinearModel;
	problem *_liblinearProblem;
	parameter *_liblinearParameter;

	void _freeSvmModel();

	void _freeSvmProblem();

	void _freeSvmParameter();

	void _setSvmProblem(bool clearAddedData = false);

	void _setSvmParameter(const Parameter &parameter);

	static svm_node _makeSvmNode(const std::pair<int, double> &pair);

	svm_node *_sparseArrayToArrayOfSvmNodes(const FMConverter::SparseArray &arr) const;

};
