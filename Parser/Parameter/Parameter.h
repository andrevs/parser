#pragma once

#include <string>
#include <vector>
#include <map>


class Parameter {
public:
	typedef std::vector<std::string> ParamBaseType;

	Parameter(const std::string &path);

	void readCommandLineArgs(int argc, char **argv);

	bool paramIsDefined(const std::string &name) const;

	const std::vector<std::string> &getParamAsVectorOfStrings(const std::string &name) const;

	const std::string &getParamAsString(const std::string &name) const;
	
	int getParamAsInt(const std::string &name) const;

	double getParamAsDouble(const std::string &name) const;

private:
	std::map<std::string, ParamBaseType> _index;

	void _updateIndex(const std::vector<std::string> &params);

};
