
#include "Parameter.h"

#include <cstdlib>
#include <fstream>
#include <iterator>
#include <string>
#include <vector>

#include "Functions.h"


Parameter::Parameter(const std::string &path) {
	std::ifstream configFile(path.c_str());
	std::string temp((std::istreambuf_iterator<char>(configFile)), std::istreambuf_iterator<char>());
	std::vector<std::string> params = split(temp, '\n');
	_updateIndex(params);
}


void Parameter::readCommandLineArgs(int argc, char **argv) {
	std::vector<std::string> params;
	for (int i = 0; i < argc; ++i)
		params.push_back(argv[i]);
	_updateIndex(params);
}


bool Parameter::paramIsDefined(const std::string &name) const {
	return (_index.find(name) != _index.end());
}


const std::vector<std::string> &Parameter::getParamAsVectorOfStrings(const std::string &name) const {
	return at(_index, name);
}


const std::string &Parameter::getParamAsString(const std::string &name) const {
	return at(_index, name).at(0);
}


int Parameter::getParamAsInt(const std::string &name) const {
	return atoi(at(_index, name).at(0).c_str());
}


double Parameter::getParamAsDouble(const std::string &name) const {
	return atof(at(_index, name).at(0).c_str());
}


void Parameter::_updateIndex(const std::vector<std::string>& params) {
	std::string curParamName;
	for (std::vector<std::string>::const_iterator it = params.begin(); it != params.end(); ++it)
		if (it->at(0) == '-') {
			int j = 0;
			while (it->at(j) == '-')
				++j;
			curParamName = it->substr(j);
			_index[curParamName] = ParamBaseType();
		}
		else if (!it->empty() && (it->at(0) != '#') && !curParamName.empty())
			_index[curParamName].push_back(*it);
}
