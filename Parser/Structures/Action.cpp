
#include "Action.h"

#include <string>

#include "FeatureModel.h"


Action::Action(const FeatureModel::LinkFeatures &features, bool makeLink, const std::string &deprel)
	: features(features), label(makeLink, deprel) {
}
