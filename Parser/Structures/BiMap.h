#pragma once

#include <map>

#include "Functions.h"


template<typename TLeft, typename TRight>
class BiMap {
public:

	void insert(const TLeft &left, const TRight &right);

	const TRight &getByLeft(const TLeft &left) const;

	const TLeft &getByRight(const TRight &right) const;

	size_t countLeft(const TLeft &left) const;

	size_t countRight(const TRight &right) const;

	void eraseLeft(const TLeft &left);

	void eraseRight(const TRight &right);

	void clear();

	size_t size() const;

	typedef typename std::map<TLeft, TRight>::const_iterator ConstLeftIterator;
	typedef typename std::map<TLeft, TRight>::iterator LeftIterator;

	ConstLeftIterator leftBegin() const;

	LeftIterator leftBegin();

	ConstLeftIterator leftEnd() const;

	LeftIterator leftEnd();

	ConstLeftIterator findLeft(const TLeft &left) const;

	LeftIterator findLeft(const TLeft &left);

	typedef typename std::map<TRight, TLeft>::const_iterator ConstRightIterator;
	typedef typename std::map<TRight, TLeft>::iterator RightIterator;

	ConstRightIterator rightBegin() const;

	RightIterator rightBegin();

	ConstRightIterator rightEnd() const;

	RightIterator rightEnd();

	ConstRightIterator findRight(const TRight &right) const;

	RightIterator findRight(const TRight &right);

private:
	std::map<TLeft, TRight> _lr;
	std::map<TRight, TLeft> _rl;
};


template<typename TLeft, typename TRight>
void BiMap<TLeft, TRight>::insert(const TLeft &left, const TRight &right) {
	if (_lr.find(left) != _lr.end() || _rl.find(right) != _rl.end())
		throw std::logic_error("element collision");
	_lr[left] = right;
	_rl[right] = left;
}


template<typename TLeft, typename TRight>
const TRight &BiMap<TLeft, TRight>::getByLeft(const TLeft &left) const {
	return at(_lr, left);
}


template<typename TLeft, typename TRight>
const TLeft &BiMap<TLeft, TRight>::getByRight(const TRight &right) const {
	return at(_rl, right);
}


template<typename TLeft, typename TRight>
size_t BiMap<TLeft, TRight>::countLeft(const TLeft &left) const {
	return _lr.count(left);
}


template<typename TLeft, typename TRight>
size_t BiMap<TLeft, TRight>::countRight(const TRight &right) const {
	return _rl.count(right);
}


template<typename TLeft, typename TRight>
void BiMap<TLeft, TRight>::eraseLeft(const TLeft &left) {
	if (countLeft(left)) {
		_rl.erase(_lr[left]);
		_lr.erase(left);
	}
}


template<typename TLeft, typename TRight>
void BiMap<TLeft, TRight>::eraseRight(const TRight &right) {
	if (countRight(right)) {
		_lr.erase(_rl[right]);
		_rl.erase(right);
	}
}


template<typename TLeft, typename TRight>
void BiMap<TLeft, TRight>::clear() {
	_lr.clear();
	_rl.clear();
}


template<typename TLeft, typename TRight>
size_t BiMap<TLeft, TRight>::size() const {
	if (_lr.size() != _rl.size())
		throw std::logic_error("invalid BiMap<TLeft, TRight>");
	return _lr.size();
}


template<typename TLeft, typename TRight>
typename BiMap<TLeft, TRight>::ConstLeftIterator BiMap<TLeft, TRight>::leftBegin() const {
	return _lr.begin();
}


template<typename TLeft, typename TRight>
typename BiMap<TLeft, TRight>::LeftIterator BiMap<TLeft, TRight>::leftBegin() {
	return _lr.begin();
}


template<typename TLeft, typename TRight>
typename BiMap<TLeft, TRight>::ConstLeftIterator BiMap<TLeft, TRight>::leftEnd() const {
	return _lr.end();
}


template<typename TLeft, typename TRight>
typename BiMap<TLeft, TRight>::LeftIterator BiMap<TLeft, TRight>::leftEnd() {
	return _lr.end();
}


template<typename TLeft, typename TRight>
typename BiMap<TLeft, TRight>::ConstLeftIterator BiMap<TLeft, TRight>::findLeft(const TLeft &left) const {
	return _lr.find(left);
}


template<typename TLeft, typename TRight>
typename BiMap<TLeft, TRight>::LeftIterator BiMap<TLeft, TRight>::findLeft(const TLeft &left) {
	return _lr.find(left);
}


template<typename TLeft, typename TRight>
typename BiMap<TLeft, TRight>::ConstRightIterator BiMap<TLeft, TRight>::rightBegin() const {
	return _rl.begin();
}


template<typename TLeft, typename TRight>
typename BiMap<TLeft, TRight>::RightIterator BiMap<TLeft, TRight>::rightBegin() {
	return _rl.begin();
}


template<typename TLeft, typename TRight>
typename BiMap<TLeft, TRight>::ConstRightIterator BiMap<TLeft, TRight>::rightEnd() const {
	return _rl.end();
}


template<typename TLeft, typename TRight>
typename BiMap<TLeft, TRight>::RightIterator BiMap<TLeft, TRight>::rightEnd() {
	return _rl.end();
}


template<typename TLeft, typename TRight>
typename BiMap<TLeft, TRight>::ConstRightIterator BiMap<TLeft, TRight>::findRight(const TRight &right) const {
	return _rl.find(right);
}

template<typename TLeft, typename TRight>
typename BiMap<TLeft, TRight>::RightIterator BiMap<TLeft, TRight>::findRight(const TRight &right) {
	return _rl.find(right);
}
