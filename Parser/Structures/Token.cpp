
#include "Token.h"

#include <cctype>
#include <istream>
#include <ostream>
#include <string>
#include <vector>

#include "Functions.h"


Token::Token(int id,
             const std::string &form,
             const std::string &lemma,
             const std::string &postag,
             const std::string &features,
             int head,
             const std::string &deprel
            )
	: id(id), form(form), lemma(lemma), postag(postag), features(features), head(head), deprel(deprel) {
}


Token::Token(const std::string &str) {
	_setFields(str);
}


bool Token::isWord() const {
	size_t i = 0;
	return (iswalnum(utf8SymbolToUnicode(form, i)) != 0);
}


std::istream &operator>>(std::istream &is, Token &token) {
	std::string str;
	std::getline(is, str);
	token._setFields(str);
	return is;
}


std::ostream &operator<<(std::ostream &os, const Token &token) {
	os << token.id
	   << '\t'
	   << token.form
	   << '\t'
	   << token.lemma
	   << '\t'
	   << token.postag
	   << '\t'
	   << token.features
	   << '\t'
	   << token.head
	   << '\t'
	   << token.deprel;
	return os;
}


// PRIVATE

void Token::_setFields(const std::string &str) {
	std::vector<std::string> splitted = split(str, '\t');
	if (splitted.size() != 7)
		throw std::logic_error("invalid string for Token");
	id = atoi(splitted[0].c_str());
	form = splitted[1];
	lemma = splitted[2];
	postag = splitted[3];
	features = splitted[4];
	head = atoi(splitted[5].c_str());
	deprel = splitted[6];
}
