#pragma once

#include <string>
#include <vector>

#include "FeatureModel.h"


class Action {
public:
	Action(const FeatureModel::LinkFeatures &features, bool makeLink, const std::string &deprel);

	FeatureModel::LinkFeatures features;
	FeatureModel::Label label;

};


typedef std::vector<Action> Actions;
