
#include "Tree.h"


Tree::Tree(int n)
	: _parents(n + 1) {
	_parents[0] = -1;
}


bool Tree::edgeIsPermissible(int from, int to) const {
	if (from == to || _indegree(to) != 0 || _isAncestor(to, from))
		return false;
	return true;
}


bool Tree::addEdge(int from, int to) {
	if (edgeIsPermissible(from, to)) {
		_parents[to] = from;
		return true;
	}
	else {
		return false;
	}
}


int Tree::parent(int vertex) const {
	return _parents[vertex];
}


std::vector<int> Tree::parents() const {
	return _parents;
}


bool Tree::_isAncestor(int potentialAncestor, int vertex) const {
	while (_parents[vertex] > 0) {
		if (_parents[vertex] == potentialAncestor)
			return true;
		vertex = _parents[vertex];
	}
	return false;
}


int Tree::_indegree(int vertex) const {
	return (_parents[vertex] != 0);
}


int Tree::_outdegree(int vertex) const {
	int outdegree = 0;
	for (std::vector<int>::const_iterator it = _parents.begin(); it != _parents.end(); ++it)
		if (*it == vertex)
			++outdegree;
	return outdegree;
}
