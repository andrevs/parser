#pragma once

#include <vector>


class Tree {
public:
	Tree(int n);

	bool edgeIsPermissible(int from, int to) const;

	bool addEdge(int from, int to);

	int parent(int vertex) const;

	std::vector<int> parents() const;

private:
	std::vector<int> _parents;

	bool _isAncestor(int potentialAncestor, int vertex) const;

	int _indegree(int vertex) const;

	int _outdegree(int vertex) const;

};
