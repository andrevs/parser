#pragma once

#include <istream>
#include <ostream>
#include <string>


class Token {
public:
	Token(int id,
	      const std::string &form,
	      const std::string &lemma,
	      const std::string &postag,
	      const std::string &features,
	      int head,
	      const std::string &deprel
		 );

	Token(const std::string &str);

	bool isWord() const;

	friend std::istream &operator>>(std::istream &is, Token &token);

	friend std::ostream &operator<<(std::ostream &os, const Token &token);

	int id;
	std::string form;
	std::string lemma;
	std::string postag;
	std::string features;
	int head;
	std::string deprel;

private:
	void _setFields(const std::string &str);

};
