
#include <algorithm>
#include <cstdlib>
#include <ctime>
#include <iostream>

#include "Sentence.h"


int main(int argc, char **argv) {
	if (argc < 3) {
		std::cerr << "Usage: \"" << argv[0] << " in.conll out.conll [seed]\"" << std::endl;
		exit(1);
	}
	
	Sentences sentences = readSentences(argv[1]);

	if (argc < 4)
		std::srand(static_cast<unsigned int>(std::time(NULL)));
	else
		std::srand(static_cast<unsigned int>(atoi(argv[3])));

	std::random_shuffle(sentences.begin(), sentences.end());

	writeSentences(argv[2], sentences);

	return 0;
}
