
#include <iostream>

#include "Sentence.h"


int main(int argc, char **argv) {
	if (argc < 3) {
		std::cerr << "Usage: \"" << argv[0] << " 1.conll [2.conll ...] out.conll\"" << std::endl;
		exit(1);
	}
	
	Sentences sentences;

	for (int i = 1; i + 1 < argc; ++i) {
		Sentences temp = readSentences(argv[i]);
		sentences.insert(sentences.end(), temp.begin(), temp.end());
	}

	writeSentences(argv[argc - 1], sentences);

	return 0;
}
