
#include <cstdlib>
#include <iostream>
#include <limits>

#include "Sentence.h"


int main(int argc, char **argv) {
	if (argc < 4) {
		std::cerr << "Usage: \"" << argv[0] << " in.conll out1.conll out2.conll [fraction]\"" << std::endl;
		exit(1);
	}

	Sentences sentences = readSentences(argv[1]);

	double fraction = 0.5;

	if (4 < argc) {
		double tempFraction = atof(argv[4]);
		if (0 + DBL_EPSILON < tempFraction && tempFraction < 1 - DBL_EPSILON) {
			fraction = tempFraction;
		}
		else {
			std::cerr << "Fraction should be from the interval (0, 1)." << std::endl;
			exit(1);
		}
	}

	size_t firstPartSize = static_cast<size_t>(sentences.size() * fraction);

	writeSentences(argv[2], Sentences(sentences.begin(), sentences.begin() + firstPartSize));
	writeSentences(argv[3], Sentences(sentences.begin() + firstPartSize, sentences.end()));

	return 0;
}
