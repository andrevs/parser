
#define _CRT_SECURE_NO_WARNINGS

#include "Functions.h"

#include <cstdio>
#include <cstdlib>
#include <string>
#include <vector>


std::vector<std::string> split(const std::string &str, char delim, int n, bool skipEmptyParts) {
	std::vector<std::string> ans;
	int lastDelimInd = -1;
	for (size_t i = 0; i < str.length(); ++i)
		if (str[i] == delim) {
			std::string temp = str.substr(lastDelimInd + 1, i - (lastDelimInd + 1));
			if (!(skipEmptyParts && temp.empty()))
				if (static_cast<int>(ans.size()) < n)
					ans.push_back(temp);
				else
					break;
			lastDelimInd = static_cast<int>(i);
		}
	std::string temp = str.substr(lastDelimInd + 1);
	if (!(skipEmptyParts && temp.empty()))
		ans.push_back(temp);
	return ans;
}


std::string intToString(int num) {
	char buf[20];
	sprintf(buf, "%d", num);
	return std::string(buf);
}


std::vector<int> stringToVectorOfInts(const std::string &str, char delim) {
	std::vector<std::string> splitted = split(str, delim);
	std::vector<int> ans;
	for (std::vector<std::string>::const_iterator it = splitted.begin(); it != splitted.end(); ++it)
		ans.push_back(atoi(it->c_str()));
	return ans;
}


std::vector<double> stringToVectorOfDoubles(const std::string &str, char delim) {
	std::vector<std::string> splitted = split(str, delim);
	std::vector<double> ans;
	for (std::vector<std::string>::const_iterator it = splitted.begin(); it != splitted.end(); ++it)
		ans.push_back(atof(it->c_str()));
	return ans;
}



std::vector<std::pair<int, double> > stringToSparseArray(const std::string &str, char delim) {
	std::vector<std::string> splitted = split(str, delim);
	std::vector<std::pair<int, double> > ans;
	for (std::vector<std::string>::const_iterator it = splitted.begin(); it != splitted.end(); ++it) {
		std::vector<std::string> pair = split(*it, ':');
		int index = atoi(pair[0].c_str());
		double value = atof(pair[1].c_str());
		ans.push_back(std::make_pair(index, value));
	}
	return ans;
}


int utf8SymbolToUnicode(const std::string &str, size_t &i) {
	int n = 7;
	unsigned char tmp = ~str[i];
	while (tmp != 0) {
		tmp >>= 1;
		--n;
	}
	int ans = str[i] & ((1 << (6 - n)) - 1);
	++i;
	for (int j = 0; j < n; ++j, ++i)
		ans = (ans << 6) | (str[i] & 0x3F);
	return ans;
}


std::vector<int> utf8ToUnicode(const std::string &str) {
	std::vector<int> ans;
	size_t i = 0;
	while (i < str.size())
		ans.push_back(utf8SymbolToUnicode(str, i));
	return ans;
}


std::string unicodeSymbolToUtf8(int symbol) {
	char buf[6] = { '\xFC' };
	int mask = 0x7C000000;
	int n = 5;
	while (n > 0 && (symbol & mask) == 0) {
		mask = (mask >> 5) & (~0x7F);
		--n;
		buf[0] <<= 1;
	}
	if (n == 0)
		buf[0] = static_cast<char>(symbol);
	else {
		buf[0] = (buf[0] | (symbol >> (6 * n))) & (~((buf[0] & -buf[0]) >> 1));
		for (int i = 0; i < n; ++i)
			buf[n - i] = 0x80 | (0x3F & (symbol >> (6 * i)));
	}
	return std::string(buf, buf + 1 + n);
}


std::string unicodeToUtf8(const std::vector<int> &unicode) {
	std::string str;
	for (size_t i = 0; i < unicode.size(); ++i)
		str += unicodeSymbolToUtf8(unicode[i]);
	return str;
}
