
#include <fstream>
#include <iomanip>
#include <ios>
#include <iostream>
#include <ostream>
#include <sstream>
#include <string>
#include <vector>

#include "Sentence.h"


class Stat {
public:
	Stat()
		: nSent(), nWrongSent(), 
		  nTokens(), nWrongTokens(), nWrongHeads(), nWrongDeprels(),
		  nWords(), nWrongWords(), nWrongWordHeads(), nWrongWordDeprels(),
		  nPuncts(), nWrongPuncts(), nWrongPunctHeads(), nWrongPunctDeprels() {
	}

	int nSent, nWrongSent;

	int nTokens, nWrongTokens, nWrongHeads, nWrongDeprels;

	int nWords, nWrongWords, nWrongWordHeads, nWrongWordDeprels;

	int nPuncts, nWrongPuncts, nWrongPunctHeads, nWrongPunctDeprels;

	friend std::ostream &operator<<(std::ostream &os, const Stat &stat);

};


bool diffTokens(const Token &test, const Token &parsed, int &nTokens, int &nWrongTokens, int &nWrongHeads, int &nWrongDeprels);


int main(int argc, char **argv) {
	if (argc < 3) {
		std::cerr << "Usage: \"" << argv[0] << " test.conll parsed.conll [diff.conll]\"" << std::endl;
		exit(1);
	}

	Sentences test = readSentences(argv[1]);
	Sentences parsed = readSentences(argv[2]);

	Stat stat;

	std::vector<std::string> diff;

	Sentences::const_iterator testIt = test.begin(), parsedIt = parsed.begin();
	for (; testIt != test.end() && parsedIt != parsed.end(); ++testIt, ++parsedIt) {
		bool diffSent = false;
		Sentence::ConstIterator t = testIt->begin(), p = parsedIt->begin();
		for (; t != testIt->end() && p != parsedIt->end(); ++t, ++p) {
			if (diffTokens(*t, *p, stat.nTokens, stat.nWrongTokens, stat.nWrongHeads, stat.nWrongDeprels)) {
				diffSent = true;
				std::stringstream temp;
				temp << std::endl << "SENTENCE #" << (testIt - test.begin()) + 1 << std::endl;
				temp << "TEST:\t\t" << *t << std::endl << "PARSED:\t\t" << *p;
				diff.push_back(temp.str());
			}
			if (t->isWord() && p->isWord())
				diffTokens(*t, *p, stat.nWords, stat.nWrongWords, stat.nWrongWordHeads, stat.nWrongWordDeprels);
			else
				diffTokens(*t, *p, stat.nPuncts, stat.nWrongPuncts, stat.nWrongPunctHeads, stat.nWrongPunctDeprels);
		}
		stat.nSent += 1;
		stat.nWrongSent += diffSent;
	}

	if (3 < argc) {
		std::ofstream file(argv[3]);
		file << std::endl << stat << std::endl << std::endl;
		for (std::vector<std::string>::const_iterator it = diff.begin(); it != diff.end(); ++it)
			file << *it << std::endl;
	}

	std::cout << stat << std::endl;

	return 0;
}


std::ostream &operator<<(std::ostream &os, const Stat &stat) {
	os << std::fixed << std::setprecision(2);
	os << "Number of sentences                      = " << stat.nSent << std::endl;
	os << "Number of wrong parsed sentences         = " << stat.nWrongSent << std::endl;
	os << "Percentage of wrong parsed sentences     = " << stat.nWrongSent * 100.0 / stat.nSent << " %" << std::endl;
	os << std::endl;
	os << "Number of tokens                         = " << stat.nTokens << std::endl;
	os << "Number of wrong parsed tokens            = " << stat.nWrongTokens << std::endl;
	os << "Percentage of wrong parsed tokens        = " << stat.nWrongTokens * 100.0 / stat.nTokens << " %" << std::endl;
	os << "Number of wrong parsed heads             = " << stat.nWrongHeads << std::endl;
	os << "Percentage of wrong parsed heads         = " << stat.nWrongHeads * 100.0 / stat.nTokens << " %" << std::endl;
	os << "Number of wrong parsed deprels           = " << stat.nWrongDeprels << std::endl;
	os << "Percentage of wrong parsed deprels       = " << stat.nWrongDeprels * 100.0 / stat.nTokens << " %" << std::endl;
	os << std::endl;
	os << "Number of words                          = " << stat.nWords << std::endl;
	os << "Number of wrong parsed words             = " << stat.nWrongWords << std::endl;
	os << "Percentage of wrong parsed words         = " << stat.nWrongWords * 100.0 / stat.nWords << " %" << std::endl;
	os << "Number of wrong parsed word heads        = " << stat.nWrongWordHeads << std::endl;
	os << "Percentage of wrong parsed word heads    = " << stat.nWrongWordHeads * 100.0 / stat.nWords << " %" << std::endl;
	os << "Number of wrong parsed word deprels      = " << stat.nWrongWordDeprels << std::endl;
	os << "Percentage of wrong parsed word deprels  = " << stat.nWrongWordDeprels * 100.0 / stat.nWords << " %" << std::endl;
	os << std::endl;
	os << "Number of puncts                         = " << stat.nPuncts << std::endl;
	os << "Number of wrong parsed puncts            = " << stat.nWrongPuncts << std::endl;
	os << "Percentage of wrong parsed puncts        = " << stat.nWrongPuncts * 100.0 / stat.nPuncts << " %" << std::endl;
	os << "Number of wrong parsed punct heads       = " << stat.nWrongPunctHeads << std::endl;
	os << "Percentage of wrong parsed punct heads   = " << stat.nWrongPunctHeads * 100.0 / stat.nPuncts << " %" << std::endl;
	os << "Number of wrong parsed punct deprels     = " << stat.nWrongPunctDeprels << std::endl;
	os << "Percentage of wrong parsed punct deprels = " << stat.nWrongPunctDeprels * 100.0 / stat.nPuncts << " %";
	return os;
}


bool diffTokens(const Token &test, const Token &parsed, int &nTokens, int &nWrongTokens, int &nWrongHeads, int &nWrongDeprels) {
	bool diffHeads = (test.head != parsed.head);
	bool diffDeprels = (test.deprel != parsed.deprel);
	nTokens += 1;
	nWrongTokens += (diffHeads || diffDeprels);
	nWrongHeads += diffHeads;
	nWrongDeprels += diffDeprels;
	return (diffHeads || diffDeprels);
}
