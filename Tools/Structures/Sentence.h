#pragma once

#include <istream>
#include <map>
#include <ostream>
#include <string>
#include <vector>

#include "Token.h"


class Sentence {
public:
	Sentence(const std::vector<std::string> &vec = std::vector<std::string>());

	typedef Token Token;

	const Token &operator[](size_t i) const;

	Token &operator[](size_t i);

	bool idIsValid(int id) const;

	const Token &getTokenById(int id) const;

	Token &getTokenById(int id);

	size_t size() const;

	bool empty() const;

	void clear();

	void pushBack(const Token &token);

	typedef std::vector<Token>::iterator Iterator;
	typedef std::vector<Token>::const_iterator ConstIterator;

	ConstIterator begin() const;

	Iterator begin();

	ConstIterator end() const;

	Iterator end();

	typedef std::vector<Token>::reverse_iterator ReverseIterator;
	typedef std::vector<Token>::const_reverse_iterator ConstReverseIterator;

	ConstReverseIterator rBegin() const;

	ReverseIterator rBegin();

	ConstReverseIterator rEnd() const;

	ReverseIterator rEnd();

	bool linkIsPermissible(int fromId, int toTd) const;

	void makeLink(int fromId, int toId, const std::string& deprel);

	friend std::istream &operator>>(std::istream &is, Sentence &s);

	friend std::ostream &operator<<(std::ostream &os, const Sentence &s);

private:
	std::vector<Token> _s;

	std::map<int, size_t> _idPos;

	bool _isAncestor(int potentialAncestorId, int id) const;

	int _indegree(int id) const;

	void _setFields(const std::vector<std::string> &vec);

};




typedef std::vector<Sentence> Sentences;


Sentences readSentences(const std::string &path);


void writeSentences(const std::string &path, const Sentences &sentences);
