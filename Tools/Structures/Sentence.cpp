
#include "Sentence.h"

#include <fstream>
#include <istream>
#include <ios>
#include <ostream>
#include <string>
#include <vector>

#include "Functions.h"
#include "Token.h"


Sentence::Sentence(const std::vector<std::string> &vec) {
	_setFields(vec);
}


const Sentence::Token &Sentence::operator[](size_t i) const {
	return _s[i];
}


Sentence::Token &Sentence::operator[](size_t i) {
	return _s[i];
}


bool Sentence::idIsValid(int id) const {
	return (_idPos.find(id) != _idPos.end());
}


const Sentence::Token &Sentence::getTokenById(int id) const {
	size_t pos = at(_idPos, id);
	return _s[pos];
}


Sentence::Token &Sentence::getTokenById(int id) {
	return const_cast<Token &>(static_cast<const Sentence &>(*this).getTokenById(id));
}


size_t Sentence::size() const {
	return _s.size();
}


bool Sentence::empty() const {
	return (_s.empty() && _idPos.empty());
}


void Sentence::clear() {
	_s.clear();
	_idPos.clear();
}


void Sentence::pushBack(const Token &token) {
	if (_idPos.find(token.id) != _idPos.end())
		throw std::logic_error("id duplication");
	_s.push_back(token);
	_idPos[token.id] = _s.size() - 1;
}


Sentence::ConstIterator Sentence::begin() const {
	return _s.begin();
}


Sentence::Iterator Sentence::begin() {
	return _s.begin();
}


Sentence::ConstIterator Sentence::end() const {
	return _s.end();
}


Sentence::Iterator Sentence::end() {
	return _s.end();
}


Sentence::ConstReverseIterator Sentence::rBegin() const {
	return _s.rbegin();
}


Sentence::ReverseIterator Sentence::rBegin() {
	return _s.rbegin();
}


Sentence::ConstReverseIterator Sentence::rEnd() const {
	return _s.rend();
}


Sentence::ReverseIterator Sentence::rEnd() {
	return _s.rend();
}


bool Sentence::linkIsPermissible(int fromId, int toId) const {
	if (fromId == toId || _indegree(toId) != 0 || _isAncestor(toId, fromId))
		return false;
	return true;
}


void Sentence::makeLink(int fromId, int toId, const std::string& deprel) {
	if (linkIsPermissible(fromId, toId)) {
		Token &to = getTokenById(toId);
		to.head = fromId;
		to.deprel = deprel;
	}
	else {
		throw std::logic_error("link is not permissible");
	}
}


std::istream &operator>>(std::istream &is, Sentence &s) {
	std::vector<std::string> vec;
	std::string temp;
	while (std::getline(is, temp) && !temp.empty())
		vec.push_back(temp);
	s._setFields(vec);
	if (is.eof() && is.fail() && !is.bad() && !s.empty())
		is.clear(std::ios::eofbit);
	return is;
}


std::ostream &operator<<(std::ostream &os, const Sentence &s) {
	for (size_t i = 0; i < s.size(); ++i) {
		os << s[i];
		if (i + 1 < s.size())
			os << std::endl;
	}
	return os;
}


// PRIVATE

bool Sentence::_isAncestor(int potentialAncestorId, int id) const {
	for (int cur = getTokenById(id).head; cur > 0; cur = getTokenById(cur).head)
		if (cur == potentialAncestorId)
			return true;
	return false;
}


int Sentence::_indegree(int id) const {
	return (getTokenById(id).head != 0);
}


void Sentence::_setFields(const std::vector<std::string> &vec) {
	clear();
	for (std::vector<std::string>::const_iterator it = vec.begin(); it != vec.end(); ++it)
		pushBack(Token(*it));
}




Sentences readSentences(const std::string &path) {
	std::ifstream file(path.c_str());
	Sentences ans;
	Sentence s;
	while (file >> s)
		ans.push_back(s);
	return ans;
}


void writeSentences(const std::string &path, const Sentences &sentences) {
	std::ofstream file(path.c_str());
	for (size_t i = 0; i < sentences.size(); ++i) {
		file << sentences[i] << std::endl;
		if (i + 1 < sentences.size())
			file << std::endl;
	}
}
