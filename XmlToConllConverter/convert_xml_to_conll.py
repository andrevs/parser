
from sys import argv

xml_file = open(argv[1], 'r')

conll_file = open(argv[2], 'w')

from xml.etree import ElementTree

tree = ElementTree.parse(xml_file)

xml_sentences = tree.getroot().find('body')

from Sentence import Sentence

for i in range(len(xml_sentences)):
    sentence = Sentence(xml_sentences[i])
    conll_file.write(unicode(sentence).encode('utf-8') + '\n')
    if i + 1 < len(xml_sentences):
        conll_file.write('\n')
