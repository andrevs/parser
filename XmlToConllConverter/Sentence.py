
from xml.etree.ElementTree import Element


class Token:
    def __init__(self, arg):
        if type(arg).__name__ == 'Element':
            self.id = int(arg.attrib['ID'])
            self.form = arg.text
            self.lemma = arg.attrib['LEMMA']
            temp = arg.attrib['FEAT'].split(' ')
            self.postag = temp[0] or '_'
            self.features = '|'.join(temp[1:]) or '_'
            temp = arg.attrib['DOM']
            self.head = int('0' if temp == '_root' else temp)
            self.deprel = arg.attrib.get('LINK') or '_'
        else:
            temp = arg.split('\t')
            self.id = int(temp[0])
            self.form = temp[1]
            self.lemma = temp[2]
            self.postag = temp[3]
            self.features = temp[4]
            self.head = int(temp[5])
            self.deprel = temp[6]

    def __unicode__(self):
        format_string = u'%d\t%s\t%s\t%s\t%s\t%d\t%s'
        elements = (self.id, self.form, self.lemma, self.postag, self.features, self.head, self.deprel)
        return format_string % elements


class Sentence:
    def __init__(self, element):
        self.tokens = []
        id_index = dict([(0, 0)])
        self._parse_tail(element.text)
        for child in element:
            if child.tag == 'W':
                self.tokens += [Token(child)]
                cur_ind = len(self.tokens) - 1
                cur_new_id = len(self.tokens)
                self._parse_tail(child.tail)
                id_index[self.tokens[cur_ind].id] = cur_new_id
                self.tokens[cur_ind].id = cur_new_id
        for i in range(len(self.tokens)):
            self.tokens[i].head = id_index[self.tokens[i].head]

    def __unicode__(self):
        ans = u''
        for token in self.tokens:
            ans += unicode(token) + '\n'
        return ans[:-1]

    @staticmethod
    def _no_space_chars(s):
        ans = []
        for c in s:
            if not c.isspace():
                ans += [c]
        return ans

    @staticmethod
    def _make_token(c, id, head):
        format_string = '%d\t%s\t%s\t%s\t%s\t%d\t%s'
        elements = (id, c, c, '_', '_', head, '_')
        return Token(format_string % elements)

    def _parse_tail(self, tail):
        prev_chars, next_chars = tail.split('\n')
        prev_word_id = self.tokens[-1].id if self.tokens else -1
        for c in self._no_space_chars(prev_chars):
            self.tokens += [self._make_token(c=c, id=len(self.tokens) + 1, head=prev_word_id)]
        next_word_id = max(1, prev_word_id + 1)
        for c in self._no_space_chars(next_chars):
            self.tokens += [self._make_token(c=c, id=len(self.tokens) + 1, head=next_word_id)]
